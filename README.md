IOC Check for EGI Incidents #EGI20200421 and #EGI2020512
========================================================

These scripts check for known indicators of compromise for the [EGI
Incidents #EGI20200421 and
#EGI2020512](https://csirt.egi.eu/academic-data-centers-abused-for-crypto-currency-mining/)
on any Unix system you can reach by SSH.

How it works
------------

`ioc-local-check.pl` is the actual check which runs on the system
being checks. It checks for known file IOCs and Linux kernel module
IOCs as well as for generic Diamorphine based kernel modules of
unknown name. It makes output to STDOUT and exits with exit-code ≠ 0
if it found something. (If `-v` for verbose is given, it always
outputs something.)

`ioc-remote-check.sh` copies `ioc-local-check.pl` to all to-be-checked
systems via scp, calls it, and removes it again. Currently only
supports root logins via SSH. The usage of SSH keys for logging in is
assumed. It also checks if the script copied to the remote host has
been modified (via `sha256sum`) and does not delete it at the end of
the check it in that case. Exits with exit-code ≠ 0 if anything is
found on one of the host.

### Commandline Options

Both commands understand the `-v` and `-d` commandline options:

* `-v` makes the output verbose: It adds a summary. (By default no
  output means the host is clean as common in Unix philosophy.`)
  
* `-d` adds debug output: It adds checks for files and directories
  which are commonly there on most Linux systems. They belong to the
  fictional incident "DEBUG".

`ioc-remote-check.sh` also knows `-a`:

* `-a` (mnemonic: "all" or "add") causes both, the hosts on the
  commandline and the hosts in the file `hosts` to be checked.

### Configuration of Hosts to Scan

If any host is given to `ioc-remote-check.sh` on the commandline, it
checks only these. (This is a change from the initial release.)

If none is given or the option `-a` is given,`ioc-remote-check.sh`
looks for a file named `hosts` in the current directory. The file
format is one host per line. Unix-style comments (lines starting with
`#`) are supported and hence such lines are ignored. Blank lines are
ignored, too.

If neither a file exists nor hostnames are given on the
commandline. only `localhost` is checked.


Requirements
------------

### Admin Workstation

`ioc-remote-check.sh` uses only these standard POSIX commands and SSH:

* `sh` (a bourne compatible shell like `ash` or `dash` suffices, `bash` is not needed.)
* `egrep`
* `test` (as `[`)
* `sha256sum` (GNU Coreutils) or `shasum` (Perl, used on macOS)
* `which` to figure out which of the tools above to use.
* `awk` (`mawk` suffices, no `gawk` needed.)
* `scp` (needs to understand options `-q` and `-p`) and `ssh` (needs
  to understand options `-a` and `-x`) — OpenSSH is known to work.

### To Be Checked Hosts

`ioc-local-check.pl` uses only these standard POSIX commands and Perl:

* Perl 5, no modules required.
* `sha256sum` (always)
* `getent`
* `awk` (`mawk` suffices, no `gawk` needed.)
* `hostname`
* `kill`
* `lsmod`
* `sort`
* `diff`
