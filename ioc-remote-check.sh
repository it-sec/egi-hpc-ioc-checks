#!/bin/sh

# Figuring out which hosts to scan
params=''
addhosts=0
for host in "$@"; do
    if [ "$host" = '-v' -o "$host" = "-d" ]; then
        params="$params $host"
    elif [ "$host" = '-a' ]; then
        addhosts=1
    else
        hosts="$hosts $host";
    fi
done

if [ -f hosts -a '(' "$addhosts" = 1 -o -z "$hosts" ')' ]; then
    hosts="$hosts $(egrep -v '^#' hosts)";
fi

if [ -z "$hosts" ]; then
    hosts=localhost
fi

# Generate the local checksum
localhashsumtool=''
if which sha256sum > /dev/null; then
    localhashsumtool=sha256sum
elif which shasum; then
    localhashsumtool="shasum -a256"
else
    echo "Neither sha256sum nor shasum locally found" 1>&2;
    exit 255
fi

hash=$($localhashsumtool ioc-local-check.pl | awk '{print $1}')
globalexit=0
for host in $hosts; do
    scp -qp ioc-local-check.pl root@$host:/tmp/
    ssh -ax root@$host '[ $(sha256sum /tmp/ioc-local-check.pl | awk '\''{print $1}'\'') = '"$hash"' ] && perl /tmp/ioc-local-check.pl '"$params"'; RC=$?; if [ $(sha256sum /tmp/ioc-local-check.pl | awk '\''{print $1}'\'') != '"$hash"' ]; then echo "$(hostname): WARNING: /tmp/ioc-local-check.pl modified, not deleting"; else rm /tmp/ioc-local-check.pl; fi; exit $RC'
    RC=$?
    globalexit=$(echo ${globalexit}+${RC} | bc)
done

exit $globalexit
