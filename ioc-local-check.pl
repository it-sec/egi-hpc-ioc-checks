#!/usr/bin/env perl

use strict;
use warnings;

# IOCs from https://csirt.egi.eu/academic-data-centers-abused-for-crypto-currency-mining/
#
# Removed due to false positives:
#    /usr/bin/on_ac_power        EGI20200421
#    /usr/share/sos/             EGI20200421

my %ioc = qw(
    /home/*/.mozilla/xdm        EGI20200421
    /tmp/.dbs*                  EGI20200421
    /tmp/.lock                  EGI20200421
    /tmp/aes.tgz                EGI20200421
    /tmp/db.tgz                 EGI20200421
    /tmp/dbsyn*                 EGI20200421
    /tmp/reserved               EGI20200421
    /tmp/systemdb               EGI20200421
    /tmp/updatedb               EGI20200421
    /tmp/check_power            EGI20200421
    /tmp/hdshare                EGI20200421
    /tmp/readps                 EGI20200421
    /usr/lib/libocs.so          EGI20200421
    /usr/lib64/.lib/l64         EGI20200421
    /usr/share/aldi.so          EGI20200421
    /usr/share/sos/rh.pub       EGI20200421
    /var/tmp/.lock              EGI20200421
    /var/tmp/.lock/clogs        EGI20200421
    /var/tmp/.lock/cpa.h        EGI20200421
    /var/tmp/.lock/ologs        EGI20200421
    /wlcg/arc-ce1/cache/.cache  EGI20200421

    /apps/.ior/read/.terma         EGI2020512
    /apps/.ior/read/.termb         EGI2020512
    /etc/fonts/.fonts              EGI2020512
    /etc/fonts/.low                EGI2020512
    /etc/terminfo/.terma           EGI2020512
    /etc/terminfo/.termb           EGI2020512
    $HOME/.mozilla/plugins/.fonts  EGI2020512
    $HOME/.mozilla/plugins/.low    EGI2020512
    $HOME/.mozilla/plugins/.aa     EGI2020512
    $HOME/.mozilla/plugins/test    EGI2020512
    /usr/lib64/.lib/l64            EGI2020512
    /var/games/.terma              EGI2020512
    /var/games/.termb              EGI2020512

    /tmp/          DEBUG
    /run/udev.pid  DEBUG
    /run/lock/*    DEBUG
);

# Hostname for reporting
my $hostname = `hostname`;
chomp($hostname);

# Commandline parsing
my $debug = 0;
my $verbose = 0;
foreach my $param (@ARGV) {
    if ($param eq '-d' or $param eq '--debug') {
        $debug = 1;
    }
    if ($param eq '-v' or $param eq '--verbose') {
        $verbose = 1;
    }
}

my $found = 0;
foreach my $glob (sort keys %ioc) {
    my $incident = $ioc{$glob};

    # Handle $HOME in globs to expand it to any user present on the system.
    if ($glob =~ /\$HOME/) {
        my @homes = `getent passwd | awk -F: '{print \$6}'`;
        my @newglobs = ();
        foreach my $home (@homes) {
            chomp($home);
            my $globwithhome = $glob;
            $globwithhome =~ s/\$HOME/$home/;
            push(@newglobs, $globwithhome);
        }
        $glob = join(' ', @newglobs);
    }
    my @files = glob($glob);
    foreach my $file (@files) {
        if (-e $file) {
            unless ($incident eq 'DEBUG' and $debug != 1) {
                print "$hostname $incident: ".`ls -ld '$file'`;
                $found++;
            }
        }
    }
}

# Check /etc/cron.hourly/0anacron for not being touched (RPM-only)
if (-e '/etc/cron.hourly/0anacron') {
    my $rpm = `which rpm`;
    if ($rpm) {
        my @rpm_anacron = `rpm -Vvf --nomode /etc/cron.hourly/0anacron`;
        # In debug mode, output all modified anacron files, should at
        # least output some directories.
        unless ($debug) {
            @rpm_anacron = grep m(/etc/cron.hourly/0anacron\b), @rpm_anacron;
        }

        @rpm_anacron = grep !m(\Q.........    /), @rpm_anacron;

        foreach my $rpmline (@rpm_anacron) {
            print "$hostname EGI20200421: modifed anacron file: $rpmline";
            $found++;
        }
    } else {
        print "WARNING on $hostname: /etc/cron.hourly/0anacron found but not rpm.\n";
    }
}

# Check for the Diamorphine Linux Kernel Module
# (https://github.com/m0nad/Diamorphine)

# First check for known bad LKM names
# Perl's "kill(63, $$);" kills the script, so let's put a /bin/sh inbetween.
system('kill -63 $$');
my @lsmod = `lsmod`;
my @suspicious = grep /\b(diamorphine|scsi|iscsi|readaps)\b/, @lsmod;
foreach my $lkm (@suspicious) {
    print "$hostname: KNOWN SUSPICIOUS KERNEL MODULE: $lkm";
    $found++;
}

# Now do the same, just diff-based
system('kill -63 $$');
system("lsmod | sort > $0.$$.lsmod1");
system('kill -63 $$');
system("lsmod | sort > $0.$$.lsmod2");
my @diff = `diff $0.$$.lsmod1 $0.$$.lsmod2`;
unlink("$0.$$.lsmod1", "$0.$$.lsmod2");
foreach my $diffline (@diff) {
    print "UNKNOWN SUSPICIOUS KERNEL MODULE: $diffline";
    $found++ if $diffline =~ /^[<>]/;
}



# Summary
if ($verbose) {
    if ($found == 0) {
        print "$hostname: CLEAN\n";
    } else {
        print "$hostname: FOUND $found IOCs\n";
    }
}

exit $found;
